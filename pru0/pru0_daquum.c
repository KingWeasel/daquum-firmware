/*
 * Source Modified by Derek Molloy for Exploring BeagleBone Rev2
 * and again by Jacob Kunnappally for Embedded Systems Design Lab
 * Based on the examples distributed by TI
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *
 *	* Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the
 *	  distribution.
 *
 *	* Neither the name of Texas Instruments Incorporated nor the names of
 *	  its contributors may be used to endorse or promote products derived
 *	  from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdint.h>
#include <string.h>
#include <pru_cfg.h>
#include <pru_intc.h>
#include <pru_ctrl.h>
#include <rsc_types.h>
#include <pru_rpmsg.h>
#include "resource_table_0.h"

#define HOST_INT ((uint32_t)1 << 30)
#define TO_ARM_HOST 16 //16 and 17 for PRU0
#define FROM_ARM_HOST 17
#define CHAN_NAME "rpmsg-pru"
#define CHAN_DESC "Channel 30"
#define CHAN_PORT 30
#define RPMSG_MESSAGE_SIZE 8
#define VIRTIO_CONFIG_S_DRIVER_OK 4

#define SYNC_ON 0x00004000  // 0100
#define SYNC_OFF 0xFFFFBFFF // 1011
#define PPS_ON 0x00008000   // 1000
#define PPS_OFF 0xFFFF7FFF  // 0111

#define SLOTUPDATE 0x00000000
#define BASE 0x00010000 // SHARED MEM

#define EIGHTEFFS 0xFFFFFFFF
#define SIXTEENEFFS 0xFFFFFFFFFFFFFFFF
#define THIRTYBITS 0x3FFFFFFFULL
#define HALFBILLION 500000000ULL
#define ONEBILLION 1000000000ULL
#define TWOBILLION 2000000000ULL

struct pru_rpmsg_transport transport;
uint16_t src, dst, len;
volatile uint8_t *status;

volatile register uint32_t __R30; // 32 output gpios
volatile register uint32_t __R31; // 32 input gpios

uint8_t rpmsg_payload[RPMSG_MESSAGE_SIZE];

uint64_t the_time;
int nsec_per_cycle = 5;
int ftune = 0;
int pps, tdiff, tdiff_last, tdiff_lastlast = 0;
uint64_t arm_time, last_ask, nsecs, read_t0, t0, tick, tock, elapsed;

static inline uint64_t getnupdate_time();
static inline uint64_t master_time();
static inline int get_msg();

int main()
{

    volatile uint64_t **slot_update =
        (volatile uint64_t **)(BASE + SLOTUPDATE); // address of place where PRU1
                                                   // signals which tstamp to update

    // copying from RPMsg Example
    //
    CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
    CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_HOST;
    status = &resourceTable.rpmsg_vdev.status;

    while (!(*status & VIRTIO_CONFIG_S_DRIVER_OK))
        ;

    pru_rpmsg_init(&transport, &resourceTable.rpmsg_vring0,
                   &resourceTable.rpmsg_vring1, TO_ARM_HOST, FROM_ARM_HOST);

    while (pru_rpmsg_channel(RPMSG_NS_CREATE, &transport, CHAN_NAME,
                             CHAN_DESC, CHAN_PORT) != PRU_RPMSG_SUCCESS)
        ;
    //
    // end copy from RPMsg Example

    while (1)
    {
        __R30 |= SYNC_ON; // ADC starts output
        __R30 &= PPS_OFF;

        t0 = SIXTEENEFFS;
        read_t0 = SIXTEENEFFS;

        // get initial kick from ARM so that we get message channel
        // address info (src, dst, etc.)
        while (get_msg() != PRU_RPMSG_SUCCESS)
        {
        }

        PRU0_CTRL.CTRL_bit.CTR_EN = 0;
        PRU0_CTRL.CYCLE = 0;
        PRU0_CTRL.CTRL_bit.CTR_EN = 1;

        the_time = master_time();
        PRU0_CTRL.CYCLE = 0;
        last_ask = the_time;

        // PRU1 will overwrite this when it needs a tstamp
        *slot_update = (uint64_t *)EIGHTEFFS;

        while (1)
        {
            // ask ARM for time approx. every second
            if ((getnupdate_time() - last_ask) >= ONEBILLION)
            {
                // arm_time = the_time - last_ask;
                last_ask = the_time;
                tick = PRU0_CTRL.CYCLE;
                arm_time = master_time();
                tock = PRU0_CTRL.CYCLE;
                elapsed = (tock - tick) * 5;

                if (the_time < arm_time)
                {
                    tdiff = arm_time - the_time;
                    if(tdiff_last < tdiff)// diff getting bigger
                        nsec_per_cycle++;
                }
                else if (the_time > arm_time)
                {
                    tdiff = the_time - arm_time;
                    if(tdiff_last < tdiff)// diff getting bigger
                        nsec_per_cycle--;
                }

                // if (tdiff > ONEBILLION)
                // {
                //     the_time = arm_time;
                //     nsec_per_cycle--;
                // }
                tdiff_last = tdiff;
                tdiff_lastlast = tdiff_last;
            }

            // data tstamp update logic
            if (*slot_update != (uint64_t *)EIGHTEFFS)
            {
                // PRU1 wrote new address to update
                **slot_update = getnupdate_time();
                *slot_update = (uint64_t *)EIGHTEFFS; // reset
            }

            // PPS logic
            nsecs = getnupdate_time() & THIRTYBITS;
            if (nsecs >= ONEBILLION)
                nsecs -= ONEBILLION; // modulo rollover

            if ((nsecs < HALFBILLION) && (!pps))
            {
                __R30 |= PPS_ON;
                pps = 1;
            }
            else if ((nsecs >= HALFBILLION) && (pps))
            {
                __R30 &= PPS_OFF;
                pps = 0;
            }

            // check for a t0
            if (get_msg() == PRU_RPMSG_SUCCESS)
            {
                memcpy(&read_t0, rpmsg_payload, sizeof(read_t0));

                if (read_t0 == 0x0)
                {
                    // immediate sync
                    __R30 &= SYNC_OFF;
                    __delay_cycles(2000);
                    __R30 |= SYNC_ON;
                }
                else if (read_t0 == SIXTEENEFFS)
                    break; // reset routine
                else
                    t0 = read_t0; // actual t0
            }

            if (getnupdate_time() >= t0)
            {
                __R30 |= SYNC_ON; // t0
                t0 = SIXTEENEFFS;
            }
            // SYNC goes HI about 2 seconds before t0
            else if ((t0 - getnupdate_time()) <= TWOBILLION)
                __R30 &= SYNC_OFF;
        }
    }
}

static inline uint64_t getnupdate_time()
{
    the_time += (PRU0_CTRL.CYCLE * nsec_per_cycle) + ftune;
    PRU0_CTRL.CYCLE = 0;
    return the_time;
}

static inline uint64_t master_time()
{
    pru_rpmsg_send(&transport, dst, src, (void *)&the_time, sizeof(the_time));
    while (get_msg() != PRU_RPMSG_SUCCESS)
    {
    }
    return *(uint64_t *)rpmsg_payload;
}

static inline int get_msg()
{
    int result = PRU_RPMSG_NO_KICK;
    if (__R31 & HOST_INT)
    {
        memset(rpmsg_payload, 0, sizeof(rpmsg_payload));
        CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_HOST;
        result = pru_rpmsg_receive(&transport, &src, &dst, rpmsg_payload, &len);
    }
    return result;
}
