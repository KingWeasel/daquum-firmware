/*
 * Source Modified by Derek Molloy for Exploring BeagleBone Rev2
 * and again by Jacob Kunnappally for Embedded Systems Design Lab
 * Based on the examples distributed by TI
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *
 *	* Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the
 *	  distribution.
 *
 *	* Neither the name of Texas Instruments Incorporated nor the names of
 *	  its contributors may be used to endorse or promote products derived
 *	  from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdint.h>
#include <string.h>
#include <pru_cfg.h>
#include <pru_intc.h>
#include <pru_ctrl.h>
#include <rsc_types.h>
#include <pru_rpmsg.h>
#include "resource_table_1.h"

#define HOST_INT ((uint32_t)1 << 31)
#define TO_ARM_HOST 18 //18 and 19 for PRU1
#define FROM_ARM_HOST 19
#define CHAN_NAME "rpmsg-pru"
#define CHAN_DESC "Channel 31"
#define CHAN_PORT 31
#define RPMSG_MESSAGE_SIZE 4
#define VIRTIO_CONFIG_S_DRIVER_OK 4

#define SCLK_ON 0x00000004  // 0100
#define SCLK_OFF 0xFFFFFFFB // 1011
#define LO_BYTE 0x000000FF

#define SAMP_PER_SLOT 100
#define NUM_SLOTS 3
#define BASE 0x00010000       // SHARED MEM
#define SLOTUPDATE 0x00000000 // OFFSET

volatile register uint32_t __R30; // 32 output gpios
volatile register uint32_t __R31; // 32 input gpios

uint8_t rpmsg_payload[RPMSG_MESSAGE_SIZE];
struct pru_rpmsg_transport transport;
uint16_t src, dst, len;
volatile uint8_t *status;

int get_msg();

void main(void)
{
    uint32_t *slot[NUM_SLOTS]; // array of pointers
    // uint32_t msg_signal = 0;
    volatile uint32_t *slotupdate = (unsigned int *)(BASE + SLOTUPDATE);
    void *sample_addr;

    // a bit from all 8 channels is collected at a time
    // and written to memory that ARM consumes from
    // one index from bit_per_chan represents a collected
    // bit from each channel (1 byte)
    uint8_t bit_per_chan[24];
    uint32_t sample;
    int i, j, k, l, skip3;

    // set up slots in memory
    // a sample for 8 channels takes up 32 bytes (4 bytes per channel)
    // plus 8 bytes for timestamp
    int slot_size = sizeof(uint64_t) + (SAMP_PER_SLOT * 32);
    for (i = 0; i < NUM_SLOTS; i++)
        slot[i] = (void *)(BASE + sizeof(*slotupdate) +
                           sizeof(uint64_t) + (slot_size * i));

    // copying from RPMsg Example
    //
    CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
    CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_HOST;
    status = &resourceTable.rpmsg_vdev.status;

    while (!(*status & VIRTIO_CONFIG_S_DRIVER_OK))
        ;

    pru_rpmsg_init(&transport, &resourceTable.rpmsg_vring0,
                   &resourceTable.rpmsg_vring1, TO_ARM_HOST, FROM_ARM_HOST);

    while (pru_rpmsg_channel(RPMSG_NS_CREATE, &transport, CHAN_NAME,
                             CHAN_DESC, CHAN_PORT) != PRU_RPMSG_SUCCESS)
        ;
    //
    // end copy from RPMsg Example

    memset(rpmsg_payload, 0, sizeof(rpmsg_payload));

    while (1)
    {
        //reset memory
        for (i = 0; i < NUM_SLOTS; i++)
            memset(slot[i], 0, slot_size);

        __R30 &= SCLK_OFF;

        // get initial kick from ARM so that we get message channel
        // address info (src, dst, etc.)
        while (get_msg() != PRU_RPMSG_SUCCESS)
        {
        }

        // while we don't get a signal from ARM to stop
        // bit bang ADC data
        while (get_msg() != PRU_RPMSG_SUCCESS)
        {
            // for each memory slot
            for (i = 0; i < NUM_SLOTS; i++)
            {
                skip3 = 3;
                //collect SAMP_PER_SLOT samples
                for (j = 0; j < SAMP_PER_SLOT; j++)
                {
                    // way to keep high res mode but reduce f_s to ~12kHz
                    while (skip3 < 3)
                    {
                        skip3++;
                        asm("   WBC r31, 0");
                        asm("   WBS r31, 0");
                    }
                    skip3 = 0;

                    // wait for DRDY bit to clear, slightly faster
                    // response than polling at the C level
                    asm("   WBC r31, 0");

                    //write addr slot[i] to slotupdate to signal PRU0
                    //to tell ARM to put tstamp at that address
                    //PRU1 doesn't have time to wait for time, heh
                    if (j == 0)
                        *slotupdate = (uint32_t)slot[i];

                    // adc outputs 24-bit signed value, one bit
                    // at a time for each channel in parallel
                    for (k = 0; k < 24; k++)
                    {
                        __R30 |= SCLK_ON; // tick
                        __delay_cycles(2);
                        bit_per_chan[k] = (__R31 >> 3) & LO_BYTE;
                        __R30 &= SCLK_OFF; // tock
                        __delay_cycles(2);
                    }

                    // this loop is for copying acquired data into the active 
                    // slot.
                    for (k = 0; k < 8; k++) // for each channel
                    {
                        sample = 0;
                        for (l = 0; l < 24; l++) // for each byte collected
                        {
                            sample <<= 1;
                            sample |= (bit_per_chan[l] >> k) & 1;
                        }
                        sample <<= 8; // take to top of 32 bit
                                      // less steps than sign extension

                        // all samples for one channel are grouped together, then the next
                        sample_addr = (void *)((int)slot[i] +
                                               sizeof(uint64_t) + ((j << 2) + (k * 400)));

                        memcpy(sample_addr, &sample, sizeof(sample));
                    }

                    // signal ARM that data is ready
                    if (j == (SAMP_PER_SLOT - 1))
                    {
                        memcpy(rpmsg_payload, &slot[i], sizeof(slot[i]));
                        pru_rpmsg_send(&transport, dst, src, rpmsg_payload,
                                       sizeof(slot[i]));
                    }
                }
            }
        }
    }
}

int get_msg()
{
    int result = PRU_RPMSG_NO_KICK;
    if (__R31 & HOST_INT)
    {
        memset(rpmsg_payload, 0, sizeof(rpmsg_payload));
        CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_HOST;
        result = pru_rpmsg_receive(&transport, &src, &dst, rpmsg_payload, &len);
    }
    return result;
}
