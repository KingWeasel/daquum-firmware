/*
- Main process collects data and sends it to massagey
- massagey actually parses data into floating point and makes DATs and gives
    to servy
- servy serves UDP multicast data
- timey works alone. waits for signals from PRU0 to give it time
- tNOT waits for someone to connect and tell it t0 so it can write it to 
    PRU0 memory
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <sys/wait.h>
#include <time.h>
#include <math.h>
#include <endian.h>
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define MCAST_GROUP "232.200.10.100"
#define MCAST_PORT "54345"
#define T0_PORT "43234"
#define PACKET_CHUNK 1024
#define MMNP 0xBEEFCAFE
#define BACKLOG 10

#define DATA_DEVICE "/dev/rpmsg_pru31"
#define TIME_DEVICE "/dev/rpmsg_pru30"

#define BASE 0x4A310000
#define WORD_MASK 0xFFFF
#define MAP_SIZE 12288UL //10KiB
#define RPMSG_MESSAGE_SIZE 4
#define SAMP_PER_SLOT 100
#define NUM_SLOTS 4
#define THIRTYBITS 0x3FFFFFFF
#define EIGHTEFFS 0xFFFFFFFF
#define SIXTEENEFFS 0xFFFFFFFFFFFFFFFF
#define ONEBILLION 1000000000ULL

// seconds between LV 1904 epoch and DAQUUM 2020 epoch
#define LV_EPOCH_OFFSET 3660681600ULL

// seconds between Unix epoch and DAQUUM epoch
#define UNIX_EPOCH_OFFSET 1577836800ULL

struct LV_tstamp
{
    uint64_t secs;
    uint64_t frac_secs;
};

struct DAT
{
    struct LV_tstamp timestamp;
    uint64_t dt;
    int32_t data[8][SAMP_PER_SLOT];
};

// make sure PRUs stop before this stops
// so no BBB lockup
int keepRunning = 1;

// multiple processes need this so do
// the easy thing and make it global
void *map_base = 0;

const int slot_size = sizeof(uint64_t) + (32 * SAMP_PER_SLOT);
const int payload_size = sizeof(struct DAT) + 8; // + rows, cols

int collecty(int c2m_write);
int massagey(int c2m_read, int m2p_write);
int packety(int m2p_read, int p2s_write);
int servy(int p2s_read);
int timey(int t02t_read);
int t0(int t02t_write);
void sigchild_handler(int s);
void *get_in_addr(struct sockaddr *sa);
void *PRUaddr2ARMaddr(void *pru_addr);
void ctrlC(int dummy);

int main(int argc, char *argv[])
{
    int fd, doesblock, i, j;
    off_t shm_base = BASE;
    pid_t collecty_p, massagey_p, packety_p, servy_p, timey_p, t0_p;
    int pipes[4][2];

    for (i = 0; i < sizeof(pipes) / sizeof(pipes[0]); i++)
    {
        // only t0 messages shouldn't block
        doesblock = (i == 3) ? O_NONBLOCK : 0;
        if (pipe2(pipes[i], doesblock) == -1)
        {
            printf("Failed to make pipe\n");
            return -3;
        }
    }

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    // open memory
    if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
    {
        printf("Failed to open memory! Exiting...\n");
        return -1;
    }

    // request memory acess to PRU shm from the kernel
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
                    fd, shm_base);
    if (map_base == (void *)-1)
    {
        printf("Failed to get shm. Exiting...\n");
        return -2;
    }
    else
        printf("PRU memory mapped to %08X!\n", (int)map_base);

    // start timey process
    timey_p = fork();
    if (timey_p == 0)
    {
        timey(pipes[3][0]); // the read end of t02t pipe
        return 0;
    }
    usleep(100000);

    // // start t0 process
    t0_p = fork();
    if (t0_p == 0)
    {
        t0(pipes[3][1]);
        return 0;
    }
    usleep(100000);

    // start servy process
    servy_p = fork();
    if (servy_p == 0)
    {
        servy(pipes[2][0]);
        return 0;
    }
    usleep(100000);

    // start packety process
    packety_p = fork();
    if (packety_p == 0)
    {
        packety(pipes[1][0], pipes[2][1]); // read end of m2p, write end of p2s
        return 0;
    }
    usleep(100000);

    // start massagey process
    massagey_p = fork();
    if (massagey_p == 0)
    {
        massagey(pipes[0][0], pipes[1][1]); // read end of c2m, write end of m2s
        return 0;
    }
    usleep(100000);

    // start collecty process
    collecty_p = fork();
    if (collecty_p == 0)
    {
        collecty(pipes[0][1]); // the write end of c2m pipe
        return 0;
    }

    while (keepRunning)
        sleep(2);

    for (i = 0; i < sizeof(pipes) / sizeof(pipes[0]); i++)
    {
        for (j = 0; j < sizeof(pipes[0]) / sizeof(pipes[0][0]); j++)
            close(pipes[i][j]);
    }

    printf("Closed pipes\n");

    kill(servy_p, SIGINT);
    kill(packety_p, SIGINT);
    kill(massagey_p, SIGINT);
    kill(collecty_p, SIGINT);
    kill(t0_p, SIGINT);
    kill(timey_p, SIGINT);

    sleep(2);

    kill(servy_p, SIGKILL);
    kill(packety_p, SIGKILL);
    kill(massagey_p, SIGKILL);
    kill(t0_p, SIGKILL);

    printf("Done. Bye!\n");
}

int collecty(int c2m_write)
{
    void *read_addr;
    void *arm_addr;
    volatile uint64_t *slot_time;
    uint64_t old_time = 0;
    int i, bytes_writ = 0;
    uint8_t slot_bytes[slot_size], rpmsg_payload[RPMSG_MESSAGE_SIZE];

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    // open rpmsg channel
    struct pollfd rpmsg_poll;
    rpmsg_poll.fd = open(DATA_DEVICE, O_RDWR);
    if (rpmsg_poll.fd < 0)
    {
        printf("Failed to open %s\n", DATA_DEVICE);
        return -1;
    }
    //initial kick to start, payload doesn't matter
    write(rpmsg_poll.fd, rpmsg_payload, sizeof(rpmsg_payload));

    // while not SIGINT essentially
    // wait for PRU1 to say which slot has ready data
    // loop until it  has a valid tstamp (newer than a known older tstamp)
    // load in data to ARM space and give to data massager via Q
    while (keepRunning)
    {
        // PRU1 gives PRU-local address to read data
        if (read(rpmsg_poll.fd, rpmsg_payload, sizeof(rpmsg_payload)) > 0)
        {
            memcpy(&read_addr, rpmsg_payload, sizeof(read_addr));

            // translate from PRU1 mem map to ARM mem map
            arm_addr = PRUaddr2ARMaddr(read_addr);
            slot_time = (uint64_t *)arm_addr;
            // printf("%llu\n", *slot_time);

            // wait a little if time hasn't been updated
            for (i = 0; i < 100; i++)
            {
                if (*slot_time > old_time)
                {
                    // some old time to compare to later
                    old_time = *slot_time;
                    break;
                }
            }

            // only get here on received sig and non dummy tstamp
            // read SAMP_PER_SLOT samples from PRU memory
            memcpy(slot_bytes, arm_addr, slot_size);

            bytes_writ = write(c2m_write, slot_bytes, sizeof(slot_bytes));

            if (bytes_writ == 0)
                usleep(1000); // slow down, makes things necessarily lossy
            else if (bytes_writ < 0)
                continue;
        }
    }

    //final kick to stop, payload doesn't matter
    write(rpmsg_poll.fd, rpmsg_payload, sizeof(rpmsg_payload));

    /* Close the rpmsg_pru character device file */
    close(rpmsg_poll.fd);

    if (munmap(map_base, MAP_SIZE) == -1)
    {
        printf("Failed to unmap memory\n");
        return -1;
    }
    else
        printf("Unmapped memory!\n");

    printf("Collecty done\n");
    return 0;
}

int massagey(int c2m_read, int m2p_write)
{
    struct DAT dat;

    int i, j, bytes_writ = 0;
    uint8_t c2m_msg[slot_size];
    const float to_volts = 5.0 / pow(2, 32);
    const double nsecs2LVfrac = pow(2, 64) / pow(10, 9);
    double elapsed_secs, lv_flt_frac_secs, dbl_secs, dt;
    uint64_t tstamp, last_tstamp, elapsed = 0;
    uint64_t int_secs, int_nsecs, lv_int_frac_secs;
    int32_t sample;
    float scaled_sample;

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    while (keepRunning)
    {
        read(c2m_read, c2m_msg, sizeof(c2m_msg));
        tstamp = *(uint64_t *)c2m_msg;

        /**
         convert tstamp (uint64_t nanoseconds representing OS system time,
         which is relative to Unix epoch) to NI LabVIEW timestamp (8 bytes of
         seconds since 1904 epoch, and 8 bytes of fractional seconds
         where each bit represents 2^-64 seconds). This is per the definition of
         a Rob Schuler DAT cluster
        **/
        int_nsecs = tstamp & THIRTYBITS;
        if (int_nsecs >= ONEBILLION)
            int_nsecs -= ONEBILLION; // modulo rollover

        int_secs = tstamp - int_nsecs;
        dbl_secs = (double)int_secs * pow(10, -9);
        int_secs = dbl_secs;
        lv_flt_frac_secs = (double)int_nsecs * nsecs2LVfrac;
        lv_int_frac_secs = lv_flt_frac_secs;

        dat.timestamp.secs = htobe64(int_secs + (uint64_t)LV_EPOCH_OFFSET);
        dat.timestamp.frac_secs = htobe64(lv_int_frac_secs);

        elapsed = tstamp - last_tstamp;
        elapsed_secs = (double)elapsed * pow(10, -9);
        dt = elapsed_secs / (double)SAMP_PER_SLOT;
        // dt = 0.000075852;
        dat.dt = htobe64(*(uint64_t *)&dt);
        last_tstamp = tstamp;

        for (i = 0; i < 8; i++) // each channel
        {
            for (j = 0; j < SAMP_PER_SLOT; j++)
            {
                sample = *(int32_t *)(&c2m_msg[8 + (j * 4) + (i * 400)]);
                scaled_sample = sample * to_volts;
                dat.data[i][j] = htobe32(*(uint32_t *)&scaled_sample);
            }
        }

        bytes_writ = write(m2p_write, &dat, sizeof(dat));

        if (bytes_writ == 0)
            usleep(1000); // slow down, makes things necessarily lossy
        else if (bytes_writ < 0)
            continue;
    }

    printf("Massagey done\n");
    return 0;
}

int packety(int m2p_read, int p2s_write)
{
    struct DAT dat;

    void *msg_ptr;
    uint64_t *uint64ptr;
    uint32_t *uint32ptr;
    int bytes_writ;
    const uint32_t rows = 8;
    const uint32_t cols = SAMP_PER_SLOT;
    uint8_t msg[payload_size + 8]; // + MMNP, payload size
    const uint32_t rows_flip = htobe32(rows);
    const uint32_t cols_flip = htobe32(cols);
    const uint32_t payload_size_flip = htobe32(payload_size);
    const uint32_t mmnp_flip = htobe32(MMNP);

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    while (keepRunning)
    {
        read(m2p_read, &dat, sizeof(dat));

        // pack DAT network message
        msg_ptr = msg;
        uint32ptr = msg_ptr;
        *uint32ptr = mmnp_flip;
        msg_ptr += 4;
        uint32ptr = msg_ptr;
        *uint32ptr = payload_size_flip;
        msg_ptr += 4;
        uint64ptr = msg_ptr;
        *uint64ptr = dat.timestamp.secs;
        msg_ptr += 8;
        uint64ptr = msg_ptr;
        *uint64ptr = dat.timestamp.frac_secs;
        msg_ptr += 8;
        uint32ptr = msg_ptr;
        *uint32ptr = dat.dt;
        msg_ptr += 8;
        uint32ptr = msg_ptr;
        *uint32ptr = rows_flip;
        msg_ptr += 4;
        uint32ptr = msg_ptr;
        *uint32ptr = cols_flip;
        msg_ptr += 4;
        memcpy(msg_ptr, dat.data, sizeof(dat.data));

        bytes_writ = write(p2s_write, &msg, sizeof(msg));

        if (bytes_writ == 0)
            usleep(1000); // slow down, makes things necessarily lossy
        else if (bytes_writ < 0)
            continue;
    }
    printf("Packety done\n");
    return 0;
}

int servy(int p2s_read)
{
    uint8_t *msg_ptr;
    int bytes_sent, bytes_rem, bytes_to_send;
    struct addrinfo hints, *servinfo, *p;
    int ai_result, sockfd;
    uint8_t msg[payload_size + 8];

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if ((ai_result =
             getaddrinfo(MCAST_GROUP, MCAST_PORT, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo returned %s\n", gai_strerror(ai_result));
        return 1;
    }

    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1)
        {
            perror("talker: socket");
            continue;
        }
        break;
    }

    if (p == NULL)
    {
        fprintf(stderr, "couldn't create socket\n");
        return 2;
    }

    connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen);

    while (keepRunning)
    {
        read(p2s_read, &msg, sizeof(msg));

        msg_ptr = msg;
        bytes_rem = sizeof(msg);
        bytes_sent = 0;
        while (bytes_rem > 0)
        {
            bytes_to_send = (bytes_rem < PACKET_CHUNK) ? bytes_rem : PACKET_CHUNK;
            bytes_sent = send(sockfd, msg_ptr, bytes_to_send, 0);
            bytes_rem -= bytes_sent;
            msg_ptr += bytes_sent;
        }
    }

    printf("Servy done\n");
    close(sockfd);
    return 0;
}

int timey(int t02t_read)
{
    uint64_t the_time, last_time, pru_time;
    struct timespec time_holder;
    uint8_t rpmsg_payload[RPMSG_MESSAGE_SIZE * 2];

    struct sched_param sp;
    sp.sched_priority = 97;

    if (sched_setscheduler(0, SCHED_FIFO, &sp) == -1)
    {
        perror("Wasn't able to set realtime priority");
        exit(-1);
    }

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    // open rpmsg channel
    struct pollfd rpmsg_poll;
    rpmsg_poll.fd = open(TIME_DEVICE, O_RDWR);
    if (rpmsg_poll.fd < 0)
    {
        printf("Failed to open %s\n", TIME_DEVICE);
        return -1;
    }

    //initial kick to start, payload doesn't matter
    write(rpmsg_poll.fd, rpmsg_payload, sizeof(rpmsg_payload));

    while (keepRunning)
    {
        // non blocking read to check for new t0
        if (read(t02t_read, rpmsg_payload, sizeof(rpmsg_payload)) ==
            sizeof(rpmsg_payload))
            write(rpmsg_poll.fd, &rpmsg_payload, sizeof(rpmsg_payload));

        // PRU0 gives PRU-local address to update time (~1Hz)
        read(rpmsg_poll.fd, rpmsg_payload, sizeof(rpmsg_payload));
        // don't put code between read and gettime
        clock_gettime(CLOCK_REALTIME, &time_holder);
        the_time = ((uint64_t)time_holder.tv_sec - UNIX_EPOCH_OFFSET) * ONEBILLION;
        the_time += (uint64_t)time_holder.tv_nsec;

        write(rpmsg_poll.fd, &the_time, sizeof(the_time));
        printf("%lf\n", (the_time * pow(10, -9)) - (*(uint64_t *)rpmsg_payload * pow(10, -9)));
        last_time = the_time;
    }

    //final kick to reset (all 0xFFs in payload)
    memset(rpmsg_payload, 0xFF, sizeof(rpmsg_payload));
    write(rpmsg_poll.fd, rpmsg_payload, sizeof(rpmsg_payload));

    /* Close the rpmsg_pru character device file */
    close(rpmsg_poll.fd);
    printf("Timey done\n");
    return 0;
}

int t0(int t02t_write)
{
    int sockfd, new_fd;
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_storage their_addr;
    socklen_t sin_size;
    struct sigaction sa;
    int yes = 1;
    // char s[INET6_ADDRSTRLEN];
    int ai_result;

    uint8_t t0_cmd[16]; // length of LV tstamp
    uint32_t recvd_mmnp, bytes_to_read;
    uint64_t LVsecs, LVfracsecs, recvd_t0;
    double dbl_nsecs, dbl_secs, dbl_fracsecs;

    signal(SIGINT, ctrlC); // SIGINT (Ctrl+C) handling

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((ai_result = getaddrinfo("10.20.80.100", T0_PORT, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo returned %s\n", gai_strerror(ai_result));
        return 1;
    }

    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            perror("server: socket");
            continue;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            perror("setsockopt");
            exit(1);
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(sockfd);
            perror("server: bind");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    if (p == NULL)
    {
        fprintf(stderr, "server: failed\n");
        exit(1);
    }

    if (listen(sockfd, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }

    sa.sa_handler = sigchild_handler; // reaps all dead processes - see beej
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
    {
        perror("sigaction");
        exit(1);
    }

    printf("server: waiting for t0 command...\n");

    while (keepRunning)
    {
        sin_size = sizeof(their_addr);
        new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);

        if (new_fd == -1)
        {
            perror("accept");
            continue;
        }

        if (fork() == 0)
        {
            close(sockfd);

            if (recv(new_fd, &recvd_mmnp, sizeof(recvd_mmnp), 0) <
                sizeof(recvd_mmnp))
                perror("recv");

            recvd_mmnp = be32toh(recvd_mmnp);

            if (recv(new_fd, &bytes_to_read, sizeof(bytes_to_read), 0) <
                sizeof(bytes_to_read))
                perror("recv");

            bytes_to_read = be32toh(bytes_to_read);

            if (recv(new_fd, t0_cmd, bytes_to_read, 0) < sizeof(t0_cmd))
                perror("recv");

            LVsecs = be64toh(*(uint64_t *)(&t0_cmd[0]));
            LVfracsecs = be64toh(*(uint64_t *)(&t0_cmd[8]));

            dbl_fracsecs = LVfracsecs * (pow(2, -64));
            dbl_secs = LVsecs + dbl_fracsecs - LV_EPOCH_OFFSET;

            dbl_nsecs = dbl_secs * pow(10, 9);
            recvd_t0 = dbl_nsecs;

            // printf("%llu\n", recvd_t0);

            write(t02t_write, &recvd_t0, sizeof(recvd_t0));

            //put in pipe
            close(new_fd);
            exit(0);
        }

        close(new_fd);
    }

    return 0;
}

void sigchild_handler(int s)
{
    int saved_errno = errno;
    while (waitpid(-1, NULL, WNOHANG) > 0)
        ;
    errno = saved_errno;
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
        return &(((struct sockaddr_in *)sa)->sin_addr);

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

void *PRUaddr2ARMaddr(void *pru_addr)
{
    return (void *)(map_base + ((int)pru_addr & WORD_MASK));
}

void ctrlC(int dummy)
{
    keepRunning = 0;
    return;
}
