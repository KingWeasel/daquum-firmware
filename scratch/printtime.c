#include <stdio.h>
#include <time.h>
#include <unistd.h>

int main()
{
    struct timespec the_time;
    while(1)
    {
        clock_gettime(CLOCK_REALTIME, &the_time);
        printf("%d.%d\n",the_time.tv_sec, the_time.tv_nsec);
        usleep(1000);
    }
}
