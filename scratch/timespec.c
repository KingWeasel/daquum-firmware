#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <byteswap.h>

union t_union
{
    struct timespec ttime;
    uint64_t tlonglong;
    uint8_t tbytes[8];
} timenow, timeswap;

int main()
{
    printf("This is how long a timespec is: %d bytes\n",sizeof(struct timespec));

    clock_gettime(CLOCK_REALTIME, &timenow.ttime);

    printf("This is the time %d.%d\n", timenow.ttime.tv_sec, timenow.ttime.tv_nsec);
    printf("as bytes %X %X\n", timenow.ttime.tv_sec, timenow.ttime.tv_nsec);
    printf("as long %lld\n", timenow.tlonglong);
    printf("long as bytes %llX\n", timenow.tlonglong);

    for(int i = 0; i < 8; i++)
    {
        printf("%02X ", timenow.tbytes[i]);
    }
    printf("\n\n");

    timeswap.tlonglong = __bswap_64(timenow.tlonglong);
    
    printf("swap as bytes %X %X\n", timeswap.ttime.tv_sec, timeswap.ttime.tv_nsec);
    printf("swap long as bytes %llX\n", timeswap.tlonglong);

    for(int i = 0; i < 8; i++)
    {
        printf("%02X ", timeswap.tbytes[i]);
    }
    printf("\n");

}
