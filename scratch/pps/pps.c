/*
 * Source Modified by Derek Molloy for Exploring BeagleBone Rev2
 * and again by Jacob Kunnappally for Embedded Systems Design Lab
 * Based on the examples distributed by TI
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *
 *	* Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the
 *	  distribution.
 *
 *	* Neither the name of Texas Instruments Incorporated nor the names of
 *	  its contributors may be used to endorse or promote products derived
 *	  from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdint.h>
#include <string.h>
#include <pru_cfg.h>
#include <pru_intc.h>
#include <pru_ctrl.h>
#include <rsc_types.h>
#include <pru_rpmsg.h>
#include "resource_table_0.h"

#define HOST_INT ((uint32_t)1 << 30)
#define TO_ARM_HOST 16 //16 and 17 for PRU0
#define FROM_ARM_HOST 17
#define CHAN_NAME "rpmsg-pru"
#define CHAN_DESC "Channel 30"
#define CHAN_PORT 30
#define RPMSG_MESSAGE_SIZE 8
#define VIRTIO_CONFIG_S_DRIVER_OK 4

#define SYNC_ON 0x00004000  // 0100
#define SYNC_OFF 0xFFFFBFFF // 1011
#define PPS_ON 0x00008000   // 1000
#define PPS_OFF 0xFFFF7FFF  // 0111

#define SLOTUPDATE 0x00000000
#define BASE 0x00010000 // SHARED MEM

#define EIGHTEFFS 0xFFFFFFFF
#define SIXTEENEFFS 0xFFFFFFFFFFFFFFFF
#define THIRTYBITS 0x3FFFFFFFULL
#define HALFBILLION 500000000ULL
#define ONEBILLION 1000000000ULL
#define TWOBILLION 2000000000ULL

#define NSEC_PER_CYCLE 5
#define NSEC_TUNE 0

volatile register uint32_t __R30; // 32 output gpios
volatile register uint32_t __R31; // 32 input gpios

volatile uint64_t the_time;

static inline uint64_t getnupdate_time();

int main()
{
    uint64_t nsecs = 0;

    while (1)
    {
        __R30 &= PPS_OFF;
        the_time = 0;

        PRU0_CTRL.CTRL_bit.CTR_EN = 0;
        PRU0_CTRL.CYCLE = 0;
        PRU0_CTRL.CTRL_bit.CTR_EN = 1;

        while (1)
        {
            // getnupdate_time();
            // nsecs = the_time & THIRTYBITS;
            // nsecs = (nsecs > ONEBILLION) ? nsecs - ONEBILLION : nsecs;

            // __R30 = (nsecs < HALFBILLION) ? (__R30 | PPS_ON) : (__R30 & PPS_OFF);
            // __delay_cycles(100);

            __R30 |= PPS_ON;
            __delay_cycles(100000000);
            __R30 &= PPS_OFF;
            __delay_cycles(100000000);
        }
    }
}

static inline uint64_t getnupdate_time()
{
    the_time += (PRU0_CTRL.CYCLE * NSEC_PER_CYCLE) + NSEC_TUNE;
    PRU0_CTRL.CYCLE = 0;
    return the_time;
}
