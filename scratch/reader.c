#include <stdio.h>
//#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
//#include <errno.h>
//#include <signal.h>
#include <fcntl.h>
//#include <ctype.h>
//#include <termios.h>
#include <pthread.h>
//#include <sys/types.h>
#include <sys/mman.h>
#include <sys/poll.h>

#define MAP_SIZE    10240UL // = 8192 + 2048
#define DEVICE_NAME "/dev/rpmsg_pru31"
#define NUM_READS   400

int main()
{   int fd, i;
    off_t shm_base = 0x4A310000;
    off_t data_off = 0x8;
    void *addr, *map_shm;
    struct pollfd rpmsg_poll;
	uint8_t reads[NUM_READS];

    rpmsg_poll.fd = open(DEVICE_NAME, O_RDWR);
    if(rpmsg_poll.fd < 0)
    {   printf("Failed to open %s\n", DEVICE_NAME);
        return -1;
    }

    if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
    {   printf("Failed to open memory! Exiting...\n");
        return -1;
    }
    else
        printf("Opened memory!\n");

    fflush(stdout);

    map_shm = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, \
        fd, shm_base);
    if(map_shm == (void *) -1)
    {   printf("Failed to get shm. Exiting...\n");
        return -1;
    }
    else
        printf("Got shared memory @ %02X!\n", shm_base);
    
    fflush(stdout);

    //initial kick to start
	write(rpmsg_poll.fd, &reads[0], sizeof(reads[0]));

    for(i = 0; i < NUM_READS; i++)
        read(rpmsg_poll.fd, &reads[i], sizeof(reads[0]));  

    //final kick to stop
	write(rpmsg_poll.fd, &reads[0], sizeof(reads[0]));

    for(i = 0; i < (sizeof(reads) / sizeof(reads[0])); i++)
        printf("Read %d: %X\n", i, reads[i]);

	/* Close the rpmsg_pru character device file */
	close(rpmsg_poll.fd);

    if(munmap(map_shm, MAP_SIZE) == -1)
    {   printf("Failed to unmap memory\n");
        return -1;
    }
    else
        printf("Unmapped memory!\n");
}
